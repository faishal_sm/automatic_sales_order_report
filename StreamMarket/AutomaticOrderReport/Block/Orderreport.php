<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace StreamMarket\AutomaticOrderReport\Block;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\App\Filesystem\DirectoryList;
use Magento\Framework\Filesystem;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManager;

class Orderreport extends \Magento\Backend\Block\Widget\Grid\Extended
{
    private $resCollectionFactory;
    protected $priceCurrency;
    protected $currencyFactory;
    protected $_storeIds = [];
    protected $_resourceConnection;
    protected $_storeManagerModel;


    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    public function __construct(
    \Magento\Backend\Block\Template\Context $context,
    \Magento\Backend\Helper\Data $backendHelper,
    \Magento\Sales\Model\ResourceModel\Report\Order\CollectionFactory $resCollectionFactory,
    \Magento\Framework\App\Response\Http\FileFactory $fileFactory, 
    Filesystem $filesystem,
    StoreManager $storeManager,
    PriceCurrencyInterface $priceCurrency,
    \Magento\Framework\App\ResourceConnection $resourceConnection,
    ScopeConfigInterface $scopeConfig,
    \Magento\Directory\Model\CurrencyFactory $currencyFactory,
    array $data = []
    ) {
        $this->_fileFactory = $fileFactory;
        $this->priceCurrency = $priceCurrency;
        $this->directory = $filesystem->getDirectoryWrite(DirectoryList::VAR_DIR);
        parent::__construct($context,$backendHelper, $data);
        $this->resCollectionFactory = $resCollectionFactory;
        $this->_resourceConnection = $resourceConnection;
        $this->scopeConfig = $scopeConfig;
        $this->_storeManagerModel = $storeManager;
        $this->currencyFactory = $currencyFactory->create();
    }
  
    public function resetCollection(){
        $this->_collection = null;
    }

    public function getCollectionSale($storeID) {
        if ($this->_collection === null) {
            $from = date("Y-m-d", strtotime("first day of previous month"));
            $to = date("Y-m-d", strtotime("last day of previous month"));

            $filterData = array('store_ids'=>$storeID, 'report_type' => 'created_at_order', 'period_type' => 'day', 'from' => $from, 'to' => $to); 
                       
            $this->_collection = $this->resCollectionFactory->create()->setPeriod(
                            $filterData['period_type']
                    )->setDateRange(
                            $filterData['from'], $filterData['to']
                    )->addStoreFilter(
                    $filterData['store_ids']
            );       
            $this->setCollection($this->_collection);    
         }  
        return $this->_collection;
    }
    
    public function getBaseCurrencyCode($storeID)
    {
        $store = $this->_storeManagerModel->getStore($storeID);
        return $store->getBaseCurrencyCode();
    }
    
    public function getWebsiteId($storeID)
    {
        return $this->_storeManager->getStore($storeID)->getWebsiteId();
    }

        public function getRate($toCurrency)
    {
        return $this->_storeManager->getStore()->getBaseCurrency()->getRate($toCurrency);
    }

    public function getCsvDatafile($storeID) {
        
        //Get website id's of available stores
        $websiteId = $this->getWebsiteId($storeID);
        //Get currency code of available stores
        $currencyCode = $this->getBaseCurrencyCode($storeID);    
        //Get currency rate of available stores
        $currencyRate = $this->getRate($currencyCode);
        //Get currency symbol of available stores
        $displayCurrency = $this->scopeConfig->getValue('automaticorderreport/automaticorderreport/display_currency',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeID);
        if($displayCurrency == 1){
            $currencySymbol = $this->currencyFactory->load($currencyCode)->getCurrencySymbol();
        }else{
            $currencySymbol ='';
        }
        
        
        $finalCurrencyRate = $this->priceCurrency->round($currencyRate);

        $resourceCollection = $this->getCollectionSale($storeID);
        $listItems = $resourceCollection;
        $currentDate = date('Y-m-d');
        $file = 'export/'.$currentDate.'/website-id-'.$websiteId.'/store-id-'.$storeID.'/orderreport(' .$currentDate."-store-id-".$storeID. ').csv';
        $stream = $this->directory->openFile($file, 'w+');
        
        $headers = array('Interval', 'Orders', 'Sales Items', 'Sales Total', 'Invoiced', 'Refunded', 'Sales Tax', 'Sales Shipping', 'Sales Discount', 'Canceled');
   
        $stream->writeCsv($headers);
        $ordercount_sum = 0;
        $total_qty_ordered_sum = 0;
        $total_income_amount_sum = 0;
        $total_invoiced_amount_sum = 0;
        $total_refunded_amount_sum = 0;
        $total_tax_amount_sum = 0;
        $total_shipping_amount_sum = 0;
        $total_discount_amount_sum = 0;
        $total_canceled_amount_sum = 0;
        $i = 0;

        foreach ($listItems as $orderdata) {
            $i++;
            $orderdetail[$i]['Interval'] = $orderdata['period'];
            $orderdetail[$i]['Orders'] = $orderdata['orders_count'];
            $ordercount_sum += $orderdata['orders_count'];
            $orderdetail[$i]['Sales Items'] = $this->priceCurrency->round($orderdata['total_qty_ordered']);
            $total_qty_ordered_sum += $orderdata['total_qty_ordered'];
            $orderdetail[$i]['Sales Total'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_income_amount']);
            $total_income_amount_sum += $orderdata['total_income_amount']*$finalCurrencyRate;
            $orderdetail[$i]['Invoiced'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_invoiced_amount']);
            $total_invoiced_amount_sum += $orderdata['total_invoiced_amount']*$finalCurrencyRate;
            $orderdetail[$i]['Refunded'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_refunded_amount']);
            $total_refunded_amount_sum += $orderdata['total_refunded_amount']*$finalCurrencyRate;
            $orderdetail[$i]['Sales Tax'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_tax_amount']);
            $total_tax_amount_sum += $orderdata['total_tax_amount']*$finalCurrencyRate;
            $orderdetail[$i]['Sales Shipping'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_shipping_amount']);
            $total_shipping_amount_sum += $orderdata['total_shipping_amount']*$finalCurrencyRate;
            $orderdetail[$i]['Sales Discount'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_discount_amount']);
            $total_discount_amount_sum += $orderdata['total_discount_amount']*$finalCurrencyRate;
            $orderdetail[$i]['Canceled'] = $currencySymbol.$finalCurrencyRate*$this->priceCurrency->round($orderdata['total_canceled_amount']);
            $total_canceled_amount_sum += $orderdata['total_canceled_amount']*$finalCurrencyRate;
        }
            $i++;
            $orderdetail[$i]['Interval'] = "Total";
            $orderdetail[$i]['Orders'] = $ordercount_sum;
            $orderdetail[$i]['Sales Items'] = $total_qty_ordered_sum;
            $orderdetail[$i]['Sales Total'] = $currencySymbol.$total_income_amount_sum;
            $orderdetail[$i]['Invoiced'] = $currencySymbol.$total_invoiced_amount_sum;
            $orderdetail[$i]['Refunded'] = $currencySymbol.$total_refunded_amount_sum;
            $orderdetail[$i]['Sales Tax'] = $currencySymbol.$total_tax_amount_sum;
            $orderdetail[$i]['Sales Shipping'] = $currencySymbol.$total_shipping_amount_sum;
            $orderdetail[$i]['Sales Discount'] = $currencySymbol.$total_discount_amount_sum;
            $orderdetail[$i]['Canceled'] = $currencySymbol.$total_canceled_amount_sum;
        
            foreach ($orderdetail as $item){
                $stream->writeCsv($item);
            }

        $stream->unlock();
        $stream->close();
        return [
            'type' => 'filename',
            'value' => $file,
            'rm' => true
        ];
    }
                 
}

