<?php

namespace StreamMarket\AutomaticOrderReport\Model\AutomaticOrderReport\Source;

class Stores
{
        public function __construct(
            \Magento\Store\Model\System\Store $systemStore
        ) {
             $this->_systemStore = $systemStore;  
        }

        public function toOptionArray() {
            return $this->_systemStore->getStoreValuesForForm(false, true);
        }
    
}
