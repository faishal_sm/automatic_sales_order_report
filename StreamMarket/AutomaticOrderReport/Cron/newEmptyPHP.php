<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace StreamMarket\AutomaticOrderReport\Cron;

use Magento\Framework\App\Area;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\StoreRepository;

class Sales 
{
    /**
     * @var Rate
     */
    protected $_storeRepository;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    private $transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    private $inlineTranslation;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var \Magento\Framework\App\Filesystem\DirectoryList
     */
    private $directoryList;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * @var \StreamMarket\AutomaticOrderReport\Block\Orderreport
     */
    private $reportBlock;

    public function __construct(
        \StreamMarket\AutomaticOrderReport\Block\Orderreport $reportBlock,
        \Psr\Log\LoggerInterface $logger,
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,
        StoreManagerInterface $storeManager,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder,
        ScopeConfigInterface $scopeConfig,
        \Magento\Framework\App\ResourceConnection $resourceConnection,
        StoreRepository $storeRepository
    ) 
    {
        $this->reportBlock = $reportBlock;
        $this->logger = $logger;
        $this->directoryList = $directoryList;
        $this->storeManager = $storeManager;
        $this->inlineTranslation = $inlineTranslation;
        $this->transportBuilder = $transportBuilder;
        $this->scopeConfig = $scopeConfig;
        $this->_storeRepository = $storeRepository;
        $this->_resourceConnection = $resourceConnection;
    }

    protected function _getStoreIds() {
    $storeIds = [];
    // By default storeIds array contains only allowed stores
    $allowedStoreIds = array_keys($this->storeManager->getStores());
    $ids = $this->scopeConfig->getValue(
            'automaticorderreport/automaticorderreport/store_option',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
    $tids = explode(",", $ids);
    // And then array_intersect with post data for prevent unauthorized stores reports
    $storeIdsList = array_intersect($allowedStoreIds,$tids);
    // If selected all websites or unauthorized stores use only allowed
    if (empty($storeIdsList)) {
        $storeIdsList = $allowedStoreIds;
    }
    // reset array keys
    $allStoreIds = array_values($storeIdsList);
    
    return $allStoreIds;
    
    }
    
    //fuction to get website id's of available stores.
    
    public function getWebsiteId($storeID)
    {
        return $this->storeManager->getStore($storeID)->getWebsiteId();
    }
    
    public function execute() { 
        $storeID = $this->_getStoreIds();
        $key = '';
        foreach ($storeID as $key => $id) {
          //Generate CSV and reset the collection  
          $this->reportBlock->getCsvDatafile($id);
          $this->reportBlock->resetCollection();
          //Send mail according to store ID
          $this->sendMail($id);  
        }         
    }
    
    public function sendMail($storeID){
        
        //Get Website Id of available stores
        $webId = $this->getWebsiteId($storeID);
        //Get Store Recipient email address using store ID
        $storeEmailRecipient = $this->scopeConfig->getValue('automaticorderreport/automaticorderreport/emailId',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeID);
        //Get Store name using store ID
        $storeName = $this->scopeConfig->getValue('general/store_information/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeID);
        // Get store owner email address using store ID 
        $storeOwnerEmail = $this->scopeConfig->getValue('trans_email/ident_sales/name',\Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeID);
        
        $currentDate = date('Y-m-d'); 
        $filePath = $this->directoryList->getPath('var').'/export/'.$currentDate.'/website-id-'.$webId.'/store-id-'.$storeID.'/orderreport(' .$currentDate.'-store-id-'.$storeID.').csv';
        $fileName = 'orderreport(' .$currentDate.'-store-id-'.$storeID.').csv';

        $templateOptions = [
          'area' => Area::AREA_FRONTEND,
          'store' => $this->storeManager->getStore()->getId(),
        ];
         
        $message="Please Find the Attachment file about Monthly Sales Order Report.";
        $templateVars = [
          'message'   => $message
        ];  
        $fromEmail=  $storeOwnerEmail;
        $fromName = $storeName." Store";
        $from = ['email' => $fromEmail, 'name' => $fromName];
        try {
        $this->inlineTranslation->suspend();
        $to = $storeEmailRecipient;     
        
        $transport = $this->transportBuilder->setTemplateIdentifier('sales_order_report_email')
                ->setTemplateOptions($templateOptions)
                ->setTemplateVars($templateVars)
                ->setFrom($from)
                ->addTo($to)
                ->addAttachment($filePath, $fileName)               
                ->getTransport();
       $transport->sendMessage();
       $this->inlineTranslation->resume();
       } catch (\Exception $e) {
            $this->logger->debug($e->getMessage());
       }
    } 
}